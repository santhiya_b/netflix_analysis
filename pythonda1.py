import pandas as pd
#Read csv file
df = pd.read_csv("netflix_titles.csv")
#viewing a summary of data frame
print(df.info())
print("--------------------------")
#checking for null values
print(df.isna().sum())
print("--------------------------")
#data cleaning based on null values
df.duration = df.duration.fillna(df.rating)
df.fillna("missing",inplace=True)
#checking for nullvalues
print(df.isna().sum())
df.to_csv('C:\\Users\\santhiya Balachandar\\Documents\\Mallow tech\\DA\\netflix_titles_updated.csv',index=False)